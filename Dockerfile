# Dev stage
FROM node:10.16.0-alpine AS dev-stage

RUN yarn global add @quasar/cli
RUN mkdir /app
WORKDIR /app

COPY . .

ENV CHOKIDAR_USEPOLLING=false

# Build stage
FROM dev-stage AS build-stage

RUN yarn
RUN yarn build

# Prod stage
FROM nginx:1.15.7-alpine AS prod-stage

COPY --from=build-stage /app/dist/spa /usr/share/nginx/html
EXPOSE 80
CMD ['nginx', '-g', 'daemon off']
