#!/bin/sh
# Requires built app, and openssh
# This script is meant to be run from the gitlab ci deployment pipeline
# to deploy the app to my main and main-dev sites
# Ideally, in the future, I will have docker on all my servers and simply
# have to deploy a docker image to a swarm. But for now, manual sftp is the only way
set -e

# Requires ENV VAR DEPLOY_KEY_FILE
# Prep stuff
if [ -z $DEPLOY_KEY_FILE ]; then echo "No DEPLOY_KEY_FILE defined!"; exit; fi
chmod 600 "$DEPLOY_KEY_FILE"
if [ -z $DEPLOY_DIR ]; then
  DEPLOY_DIR="/var/www/main-dev/apps"
  echo "No DEPLOY_DIR defined! Using defualt of $DEPLOY_DIR"
fi
if [ -z $CI_COMMIT_SHA ]; then CI_COMMIT_SHA="$(date +%s)"; fi
APP_NAME="water-drinker"
APP_PACKAGE_DIR="$APP_NAME.$CI_COMMIT_SHA"

SSH_HOST=www.jazyserver.com
SSH_USER=www-deploy
SSH_PORT=55

# Prepare the deployment package (tarball) (Assumes in project directory, and built)
cd dist/spa
tar cf "/tmp/$APP_PACKAGE_DIR.tar" .

sftp -o StrictHostKeyChecking=no -o BatchMode=yes -P $SSH_PORT -i $DEPLOY_KEY_FILE $SSH_USER@$SSH_HOST << EOC
put "/tmp/$APP_PACKAGE_DIR.tar"
EOC

ssh -o StrictHostKeyChecking=no -o BatchMode=yes -l $SSH_USER -p $SSH_PORT -i $DEPLOY_KEY_FILE $SSH_HOST << EOC
set -e
# Extract package
mkdir -p "$APP_PACKAGE_DIR"
tar xf "$APP_PACKAGE_DIR.tar" -C "$APP_PACKAGE_DIR"

mkdir -p "$DEPLOY_DIR"
mv "$APP_PACKAGE_DIR" "$DEPLOY_DIR/$APP_PACKAGE_DIR"

# Swap live packages
cd "$DEPLOY_DIR"

set +e # Disable error capture for non existent link (eg first deployment)
OLD_PACKAGE_DIR="\$(readlink '$APP_NAME')"
set -e

ln -sfn "$APP_PACKAGE_DIR" "$APP_NAME"

# Remove old package
if [ ! -z "\$OLD_PACKAGE_DIR" ]; then rm -rf "\$OLD_PACKAGE_DIR"; fi
rm -f "~/$APP_PACKAGE_DIR.tar"
EOC
